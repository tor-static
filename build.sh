#!/bin/sh

set -e

mkdir -p _build
PREFIX="`pwd`/_build"
[ -z "$MAKE" ] && MAKE="make -j4"

HOSTFLAG=
if [ -n "$HOST" ]; then
    HOSTFLAG="--host=$HOST"
    [ -z "$CC"     ] && export CC=$HOST-gcc
    [ -z "$AR"     ] && export AR=$HOST-ar
    [ -z "$RANLIB" ] && export RANLIB=$HOST-ranlib
    [ -z "$LD"     ] && export LD=$HOST-ld
    [ -z "$STRIP"  ] && export STRIP=$HOST-strip
else
    [ -z "$CC" ] && CC=gcc
    [ -z "$STRIP"] && STRIP=strip
fi


build_zlib() {
    cd zlib
    if $CC -dumpmachine | grep -q 'w64-mingw32'; then
        TOOLPREFIX=
        [ -n "$HOST" ] && TOOLPREFIX="$HOST-"
        $MAKE -f win32/Makefile.gcc PREFIX="$TOOLPREFIX"
        $MAKE -f win32/Makefile.gcc install\
            INCLUDE_PATH="$PREFIX/include" LIBRARY_PATH="$PREFIX/lib" BINARY_PATH="$PREFIX/bin"
    else
        ./configure --prefix="$PREFIX" --static
        $MAKE
        $MAKE install
    fi
    cd ..
}

build_openssl() {
    cd openssl
    ./config --prefix="$PREFIX" no-shared no-dso no-zlib
    $MAKE build_libs
    $MAKE install_dev
    cd ..
}

build_libevent() {
    cd libevent
    ./autogen.sh
    ./configure --prefix="$PREFIX" --disable-shared --enable-static --with-pic\
        --disable-samples -disable-libevent-regress\
        "CPPFLAGS=-I$PREFIX/include" "LDFLAGS=-L$PREFIX/lib" "$HOSTFLAG"
    $MAKE
    $MAKE install
    cd ..
}

build_tor() {
    cd tor
    ./autogen.sh

    # The order of linker flags is always a major pain in the ass when linking
    # static binaries, and the tor configure script doesn't provide much
    # flexibility to influence the flag order. So, as a workaround, let's just
    # pass *all* the flags necessary for openssl. There will be some duplicate
    # link flags in the final build, but that's alright.
    # The -static is needed on MinGW in order to remove the dependency on
    # libssp and libwinpthread.
    TOR_LIBS=
    $CC -dumpmachine | grep -q 'w64-mingw32' && TOR_LIBS="-lssl -lcrypto -lcrypt32 -lgdi32 -lws2_32 -static"

    # TODO: See if we can enable some more features (seccomp, rust, zstd, lzma, scrypt (what's that used for?))
    ./configure --prefix="$PREFIX" --disable-asciidoc --disable-unittests\
        --enable-static-libevent --with-libevent-dir="$PREFIX"\
        --enable-static-openssl  --with-openssl-dir="$PREFIX"\
        --enable-static-zlib     --with-zlib-dir="$PREFIX"\
        --disable-systemd --disable-lzma --disable-zstd --disable-rust\
        --disable-seccomp --disable-libscrypt $HOSTFLAG\
        --disable-tool-name-check\
        "CPPFLAGS=-I$PREFIX/include" "LDFLAGS=-L$PREFIX/lib $LDFLAGS" "LIBS=$TOR_LIBS"
    $MAKE V=1
    $MAKE install
    cd ..
}

build_zlib
build_openssl
build_libevent
build_tor

EXT=
[ -e "$PREFIX/bin/tor.exe" ] && EXT=.exe

cp "$PREFIX/bin/tor$EXT" tor-static$EXT
$STRIP tor-static$EXT
