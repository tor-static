This project helps build a static binary for Tor.

Some inspiration is taken from the
[tor-static](https://github.com/cretz/tor-static) Go library.

# Versions

This repo builds:

- Libevent 2.1.8
- OpenSSL 1.1.1b
- Tor 0.4.0.5
- zlib v1.2.11

# Requirements

- Linux or Windows+MSYS2
- A proper C compiler toolchain.
- Make
- Autotools

For Arch Linux & MSYS2-MinGW64: `pacman -S base-devel`

# Usage

- Make sure you have all the required dependencies..
- Run `./build.sh`
- Wait.
- Discover that you suddenly have a working `tor-static` binary.

The generated binary is not *entirely* static because it still links against
your system libc. Handy scripts are included for cross-compilation:

- `x86_64-linux-musl.sh` will build a fully static binary linked against musl
  libc. Requirements: musl-based cross compiler in `/opt/cross`, you can get
  these from [musl-cross](https://bitbucket.org/GregorR/musl-cross/) or
  [musl.cc](https://musl.cc/).
- `x86_64-w64-mingw32.sh` will cross-compile a Windows binary, requires a
  mingw32 cross compiler in `/opt/cross` ([musl.cc](https://musl.cc/) has
  these).

More targets TBD.
