#!/bin/sh

set -e

rm -rf _build/ tor-static

for d in zlib tor openssl libevent
do
    git -C "$d" clean -fdx
    git -C "$d" reset --hard HEAD
done
