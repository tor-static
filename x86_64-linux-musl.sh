#!/bin/sh

export PATH="/opt/cross/x86_64-linux-musl/bin:$PATH"
export HOST=x86_64-musl-linux

export LDFLAGS=-static

exec ./build.sh
